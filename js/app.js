let jeanAccount = new Account(1500.50, 1, "Super Jean", -300);
let pierreAccount = new Account(901.60, 2, "Pierre", -12);
// let pierreAccount = new Account(901.60, "Pierre", 2, -12);

console.log(jeanAccount);
console.log(pierreAccount);

// Test de deposit
jeanAccount.deposit(5000.00);

// Test de withdraw
jeanAccount.withdraw(8000);

// Test de transfer
jeanAccount.transfer(pierreAccount, 3000.80);


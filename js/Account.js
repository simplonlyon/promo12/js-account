class Account {
    
    /**
     * @param {Float} soldeAmount 
     * @param {Integer} id 
     * @param {String} name 
     * @param {Float} limit 
     */
    constructor(soldeAmount, id, name, limit){ // qu'est ce que j'ai besoin au minimum comme infos/valeurs pour créer un compte
        this.solde = soldeAmount;
        this.id = id;
        this.customerName = name;
        this.soldeLimit = limit;
    }
    
    withdraw(amount){
        if((this.solde - amount) > this.soldeLimit){
            //this.solde = this.solde - amount;
            this.solde -= amount;
        } else {
            console.log("T'es ruiné mon pauvre " + this.customerName);
            console.log("Tu n'as que " + this.solde);
            console.log("Tu ne peux pas avoir " + (this.solde - amount) + " de découvert, ta limite est de " + this.soldeLimit);
        }

        return this.solde;
    }

    deposit(amount){
        this.solde += amount;

        return this.solde;
    }

    /**
     * @param {Account} accountBeneficiary
     * @param {Float} amount 
     */
    transfer(accountBeneficiary, amount){

        if ((this.solde - amount) > this.soldeLimit){
            this.solde -= amount;
            accountBeneficiary.solde = accountBeneficiary.solde + amount;
        } else {
            console.log("Désolé " + accountBeneficiary.customerName + ", " + this.customerName + " est fauché");   
        }
    }

    /**
     * @param {Account} accountBeneficiary
     * @param {Float} amount 
     */
    debit(accountBeneficiary, amount){  
        this.solde += amount;
        accountBeneficiary.solde -= amount;
    }

}
